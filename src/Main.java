
import domain.*;

public class Main{
    public static void main (String [] args) throws InterruptedException {
        while(true) {
            System.out.println("Comencem partida:");
            Partida Partida = new Partida();
            while (Partida.tauler.guanya() < 1 && Partida.nronda < 9) {
                Partida.ronda();
            }
            Partida.tauler.mostrartauler();
            System.out.println("Guanyador :");
            System.out.println(Partida.tauler.guanya());
        }
    }
}
