package domain;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Partida {
    public tauler tauler;
    jugador[] jugadors;
    public int nronda;


    public Partida () throws InterruptedException {
        int i = 0;
        tauler = new tauler();
        nronda = 0;
        jugadors = new jugador[2];
        String entrada;
        Scanner sc = new Scanner(System.in);
        do{
            System.out.println("Indica tipus de jugador creu [h: huma || m: maquina]");
            entrada = sc.nextLine();
            if (entrada.matches("h")){
                System.out.println("Assigna un nom:");
                entrada = sc.nextLine();
                jugadors[i] = new huma(i+1,entrada);
                System.out.println("Fet");
            }
            else if (entrada.matches("m")){
                System.out.println("Creant jugador maquina");
                int n = 0;
                while(n++ <3){
                    System.out.print(" . ");
                    TimeUnit.MILLISECONDS.sleep(500);
                }
                System.out.println("");
                jugadors[i] = new maquina(i+1);
                System.out.println("Fet");

            }
            i = ++i%2;
        }while(complert());
    }

    public void ronda(){
        System.out.print("Ronda: ");
        System.out.println(nronda);

        if (jugadors[nronda%2] instanceof huma){

            Scanner sc = new Scanner(System.in);
            tauler.mostrartauler();
            System.out.print("Indica posicio jugada [");
            if (jugadors[nronda%2].tipo == 1 ) System.out.print("Creu");
            else System.out.print("Rodona");
            System.out.print("] : Columna : ");
            int pos = 0;
            pos = sc.nextInt() - 1;
            System.out.print(" Fila : ");
            pos += (sc.nextInt()-1) * 3;
            System.out.println();
            tauler.moviment(jugadors[nronda%2].tipo,pos);
        }
        else {
            tauler.moviment(jugadors[nronda%2].tipo,((maquina)jugadors[nronda%2]).juga(tauler,nronda));
            System.out.println("Jugada Maquina : ");
            tauler.mostrartauler();
        }
        nronda++;
    }

    private boolean complert(){
        for (int i=0; i<jugadors.length; i++) {
            if (jugadors[i] == null) {
                return true;
            }
        }
        return false;
    }
}
